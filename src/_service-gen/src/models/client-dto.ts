/* tslint:disable */
export interface ClientDTO {
  id?: number;
  nom?: string;
  prenom?: string;
  age?: number;
  profession?: string;
  salaire?: number;
}
