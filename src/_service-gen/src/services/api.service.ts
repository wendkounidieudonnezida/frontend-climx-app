/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ClientDTO } from '../models/client-dto';
@Injectable({
  providedIn: 'root',
})
class ApiService extends __BaseService {
  static readonly moyenneSalaireByProfessionPath = 'appclimax/client/moyenne/{profession}';
  static readonly updateClientPath = 'appclimax/client/update';
  static readonly getClientByIdPath = 'appclimax/client/{id}';
  static readonly uploadFilePath = 'appclimax/processing/file/upload';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return successful operation
   */
  moyenneSalaireByProfessionResponse(): __Observable<__StrictHttpResponse<Map<string, number>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `appclimax/client/salaire-moyen`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });
  
    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Map<string, number>>;
      })
    );
  }
  
  /**
   * @return successful operation
   */
  moyenneSalaireByProfession(): __Observable<Map<string, number>> {
    return this.moyenneSalaireByProfessionResponse().pipe(
      __map(_r => _r.body)
    );
  }
  

  /**
   * @return successful operation
   */
  updateClientResponse(): __Observable<__StrictHttpResponse<ClientDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `appclimax/client/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ClientDTO>;
      })
    );
  }
  /**
   * @return successful operation
   */
  updateClient(): __Observable<ClientDTO> {
    return this.updateClientResponse().pipe(
      __map(_r => _r.body as ClientDTO)
    );
  }

  /**
   * @return successful operation
   */
  getClientByIdResponse(id:number): __Observable<__StrictHttpResponse<ClientDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `appclimax/client/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ClientDTO>;
      })
    );
  }
  /**
   * @return successful operation
   */
  getClientById(id:number): __Observable<ClientDTO> {
    return this.getClientByIdResponse(id).pipe(
      __map(_r => _r.body as ClientDTO)
    );
  }

  /**
   * @return successful operation
   */
  uploadFileResponse(file: File): __Observable<__StrictHttpResponse<string>> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body= formData;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `appclimax/processing/file/upload`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'text'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<string>;
      })
    );
  }
  /**
   * @return successful operation
   */
  uploadFile(file: File): __Observable<string> {
    return this.uploadFileResponse(file).pipe(
      __map(_r => _r.body as string)
    );
  }
}

module ApiService {
}

export { ApiService }
