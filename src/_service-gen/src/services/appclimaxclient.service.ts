/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ClientDTO } from '../models/client-dto';
@Injectable({
  providedIn: 'root',
})
class AppclimaxclientService extends __BaseService {
  static readonly getAllAndfindByKeyboardPath = 'appclimax/client/all';
  static readonly savePath = 'appclimax/client/save';
  static readonly deleteClientPath = 'appclimax/client/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `AppclimaxclientService.GetAllAndfindByKeyboardParams` containing the following parameters:
   *
   * - `size`:
   *
   * - `search`:
   *
   * - `page`:
   *
   * @return successful operation
   */
  getAllAndfindByKeyboardResponse(params: AppclimaxclientService.GetAllAndfindByKeyboardParams): __Observable<__StrictHttpResponse<{[key: string]: {}}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.size != null) __params = __params.set('size', params.size.toString());
    if (params.search != null) __params = __params.set('search', params.search.toString());
    if (params.page != null) __params = __params.set('page', params.page.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `appclimax/client/all`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{[key: string]: {}}>;
      })
    );
  }
  /**
   * @param params The `AppclimaxclientService.GetAllAndfindByKeyboardParams` containing the following parameters:
   *
   * - `size`:
   *
   * - `search`:
   *
   * - `page`:
   *
   * @return successful operation
   */
  getAllAndfindByKeyboard(params: AppclimaxclientService.GetAllAndfindByKeyboardParams): __Observable<{[key: string]: {}}> {
    return this.getAllAndfindByKeyboardResponse(params).pipe(
      __map(_r => _r.body as {[key: string]: {}})
    );
  }

  /**
   * @param body undefined
   * @return successful operation
   */
  saveResponse(body?: ClientDTO): __Observable<__StrictHttpResponse<ClientDTO>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `appclimax/client/save`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ClientDTO>;
      })
    );
  }
  /**
   * @param body undefined
   * @return successful operation
   */
  save(body?: ClientDTO): __Observable<ClientDTO> {
    return this.saveResponse(body).pipe(
      __map(_r => _r.body as ClientDTO)
    );
  }

  /**
   * @param id undefined
   */
  deleteClientResponse(id: number): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `appclimax/client/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  deleteClient(id: number): __Observable<null> {
    return this.deleteClientResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module AppclimaxclientService {

  /**
   * Parameters for getAllAndfindByKeyboard
   */
  export interface GetAllAndfindByKeyboardParams {
    size?: number;
    search?: string;
    page?: number;
  }
}

export { AppclimaxclientService }
