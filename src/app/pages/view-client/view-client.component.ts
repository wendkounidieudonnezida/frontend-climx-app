import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientDTO } from 'src/_service-gen/src/models';
import { ApiService } from 'src/_service-gen/src/services';

@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls: ['./view-client.component.css']
})
export class ViewClientComponent implements OnInit {

  itemClient!:ClientDTO
  constructor(
    private route:ActivatedRoute,
    private clientService:ApiService,
    private _location: Location,
  ) { }

  ngOnInit(): void {
    this.getClientById(this.route.snapshot.params['id'])
  }

  private getClientById(id:number){
    if(id){
      this.clientService.getClientById(id).subscribe(
        data=>{
          this.itemClient = data
        },
        error=>{

        }
      )
    }
  }

  backClicked(){
    this._location.back();
  }

}
