import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService, AppclimaxclientService } from 'src/_service-gen/src/services';

@Component({
  selector: 'app-page-add-clients',
  templateUrl: './page-add-clients.component.html',
  styleUrls: ['./page-add-clients.component.css']
})
export class PageAddClientsComponent implements OnInit {

  selectedFile: any = null;
  msgError=""
  loading=false

  constructor(
    private fileProcessingService:ApiService,
    private router:Router
    
  ) { }

  ngOnInit(): void {
    
  }



  onFileSelected(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const extension = this.getFileExtension(file.name);
      const supportedFormats = ['txt', 'csv', 'xml', 'json'];
  
      if (supportedFormats.includes(extension)) {
        this.msgError = ""
        this.selectedFile = file;
      } else {

        console.error('Format de fichier non supporté.');
        this.selectedFile = null;
        this.msgError = "Format de fichier non supporté! Choisir un fichier de format .txt .csv .xml ou .json "
      }
    } else {
      this.selectedFile = null;
    }
  }
  
  getFileExtension(filename: string | undefined): string {

    if (!filename) {
      return '';
    }
  
    const parts = filename.split('.');
    if (parts.length === 0) {
      return '';
    }
  
    const extension = parts.pop();
    return extension ? extension.toLowerCase() : '';
  }
  

uploadFile(){
  if (this.selectedFile){
    this.loading = true
    this.fileProcessingService.uploadFile(this.selectedFile).subscribe(
      data=>{
        this.loading = false
        this.router.navigate(['dashboard'])
      },
      error=>{
        console.log(error)
      }
    )
  }

  
  
}

fileInput = document.getElementById('file');


}
