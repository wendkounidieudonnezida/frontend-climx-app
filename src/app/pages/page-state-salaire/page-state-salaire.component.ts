import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModelSalaireMoyen } from 'src/_service-gen/src/models/model-salaire-moyen';
import { ApiService } from 'src/_service-gen/src/services';

@Component({
  selector: 'app-page-state-salaire',
  templateUrl: './page-state-salaire.component.html',
  styleUrls: ['./page-state-salaire.component.css']
})
export class PageStateSalaireComponent implements OnInit {

  salByProfession !: ModelSalaireMoyen[]

  constructor(
    private router:Router,
    private clientService:ApiService
  ) { }

  ngOnInit(): void {
    this.getSalaireByProfession()
  }

  private getSalaireByProfession() {
    this.clientService.moyenneSalaireByProfession().subscribe(
      data => {

        this.salByProfession = [];
  
        Object.entries(data).forEach(([profession, salaire]) => {
          // Créer un nouvel objet ModelSalaireMoyen
          const salaireMoyen: ModelSalaireMoyen = {
            salaire: salaire,
            profession: profession
          };
          // Ajouter l'objet à notre tableau
          this.salByProfession.push(salaireMoyen);
        });
        
      },
      error => {
      }
    );
  }
  

}
