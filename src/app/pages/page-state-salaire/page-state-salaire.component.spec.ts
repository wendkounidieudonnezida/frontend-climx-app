import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageStateSalaireComponent } from './page-state-salaire.component';

describe('PageStateSalaireComponent', () => {
  let component: PageStateSalaireComponent;
  let fixture: ComponentFixture<PageStateSalaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageStateSalaireComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageStateSalaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
