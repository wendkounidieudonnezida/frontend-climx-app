import { Component, OnInit, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ClientDTO } from 'src/_service-gen/src/models';
import { AppclimaxclientService } from 'src/_service-gen/src/services';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  listeClient !: ClientDTO []
  search = '';
  size = 20;
  page = 1;
  totalItems = 0
  clientToDelete: any;

  constructor(
    private elementRef: ElementRef,
    private router:Router,
    private clientService:AppclimaxclientService,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "../assets/js/main.js";
    this.elementRef.nativeElement.appendChild(s);

    this.getAllClientByPage()

  }

  private getAllClientByPage() {

    const params = this.getRequestParams( this.page, this.size,this.search);

    this.clientService.getAllAndfindByKeyboard(params).subscribe(

      response=>{
        this.listeClient = (response as any).clientList;
        this.totalItems = (response as any).totalItems ;
      },

      error=>{
        console.log(error)
      }
    )
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.getAllClientByPage();
  }

  onKey(event:Event){
    this.search = (event.target as HTMLInputElement).value
    this.getAllClientByPage()

  }

  onViewPlus(client:ClientDTO){
    this.router.navigateByUrl(`page-client-view/${client.id}`);  
  }

  private getRequestParams( page: number, pageSize: number,search:string): any {
    let params: any = {};

    if (page) {
      params[`page`] = page - 1;
    }

    if (pageSize) {
      params[`size`] = pageSize;
    }
    if (pageSize) {
      params[`search`] = search;
    }
    return params;
  }

  delete(id:any){
    
  }



}
